# see https://docs.docker.com/docker-hub/builds/advanced/

BRANCH 	?=$(shell git symbolic-ref -q --short HEAD || git describe --tags --exact-match)
SOURCE_COMMIT ?=$(shell git rev-parse HEAD)
DOCKER_TAG = latest

PRIVATE_DOCKER_ACCOUNT=nicolas-law
DOCKER_ACCOUNT=$(PRIVATE_DOCKER_ACCOUNT)
DOCKER=docker

ifeq ($(BRANCH), master)
    $(error "You can not build from master branch")
else ifeq ($(BRANCH), develop)
	timestamp:=$(shell date "+%Y%m%d")
	DOCKER_TAG=develop-$(timestamp)
	DOCKER_ACCOUNT=$(PRIVATE_DOCKER_ACCOUNT)
else ifeq (tag, $(shell if echo "$(BRANCH)" | grep -qE '^v[0-9]+.[0-9]+.[0-9]+$$'; then echo "tag"; fi))
	DOCKER_TAG=$(subst v,,$(BRANCH))
	DOCKER_ACCOUNT=$(PRIVATE_DOCKER_ACCOUNT)
	DOCKER_LATEST_TAG=yes
else
#'/' not allowed in tag name by docker
	DOCKER_TAG:="$(subst /,-,$(BRANCH))"
	DOCKER_ACCOUNT=$(PRIVATE_DOCKER_ACCOUNT)
endif

all: clean build-front push-front

.PHONY: clean
clean: clean-front ## Clean

check-status: # avoid building images with non-versioned sources for public release
ifeq ($(DOCKER_ACCOUNT), $(PUBLIC_DOCKER_ACCOUNT))
    ifneq ($(shell git status --porcelain),)
        $(error "Working directory is not clean, commit your changes first (or stash them).")
    endif
endif

clean-front: ; $(info clean ui...)
ifeq (,$(DOCKERFILE_PATH))
	@docker rmi -f jenkins-ci:${DOCKER_TAG}
endif

.PHONY: build-front
build-front: clean-front check-status ; $(info build ui...) @ ## Build ui
	@git checkout $(BRANCH)

	@$(DOCKER) build -t jenkins-ci:$(DOCKER_TAG) \
	--no-cache \
	--rm \
	-f Dockerfile .

.PHONY: push-front
push-front: ; $(info push front image to docker hub ...) @ ## Push front image to docker hub
	@$(DOCKER) tag jenkins-ci:${DOCKER_TAG} ${DOCKER_ACCOUNT}/jenkins-ci:$(DOCKER_TAG)
	@$(DOCKER) push ${DOCKER_ACCOUNT}/jenkins-ci:$(DOCKER_TAG)
ifeq ($(DOCKER_LATEST_TAG), yes)
	@$(DOCKER) tag $(DOCKER_ACCOUNT)/jenkins-ci:$(DOCKER_TAG) $(DOCKER_ACCOUNT)/jenkins-ci:latest
	@$(DOCKER) push $(DOCKER_ACCOUNT)/jenkins-ci:latest
endif

.PHONY: help
help:
	@grep -E '^[ a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | \
		awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}'
