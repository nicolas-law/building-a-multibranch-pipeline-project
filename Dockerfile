FROM node:6-alpine AS builder
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

FROM nginx:alpine AS dist
COPY --from=builder /app/build/ /var/www/public
USER nginx